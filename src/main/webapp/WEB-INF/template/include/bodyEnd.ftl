<#macro bodyEnd >
	<script src="https://cdn.bootcss.com/jquery/2.2.3/jquery.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="https://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
	<script src="https://cdn.bootcss.com/jquery.form/3.51/jquery.form.js"></script>
   <#nested>
 </body>
 </html>
</#macro>