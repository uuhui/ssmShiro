<#global base = request.contextPath />
<#macro head title="首页">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>ssmShiro--${title!"权限管理"}</title>
    <meta name="keywords" content="项目信息">
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
   　　
    <#nested>
    <script>
  	var cfg={ctx:"${base}"};//定义上下文变量
  </script>
</head>
</#macro>