<#import "../include/header.ftl" as header/>
<#import "../include/bodyEnd.ftl" as body/>
<@header.head title="登录页面">
 <link href="${base}/resources/jquery-filter/jquery-multiple-filter.css" rel="stylesheet">
	  <link href="${base}/resources/css/index.css" rel="stylesheet">
	   <link href="${base}/resources/dist/css/bootstrap-datetimepicker.css" rel="stylesheet">
	   <link href="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.css" rel="stylesheet">
	   <link rel="stylesheet" href="https://cdn.bootcss.com/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
</@header.head>
<body class="hold-transition skin-blue-light layout-top-nav">
	<div class="wrapper">
		<div class="container">
	 	    <form  id="defaultForm" action="${base}/user/login" method="post" class="form-horizontal bv-form"  >
	 	        <div class="form-group has-feedback"></div>
		    	<div class="form-group has-feedback">
		    		<label class="col-lg-3 control-label"></label>
			      	<label id="msg" class="col-lg-4 control-label" style="color: red;">${msg!""}</label>
			      </div>
		    	<div class="form-group has-feedback">
		          <label class="col-lg-3 control-label">账号</label>
		          <div class="col-lg-9">
		              <input data-bv-field="username" class="form-control" name="username" placeholder="请输入账号" id="loginName" type="text">
		          </div>
		      </div>
		      <div class="form-group has-feedback"></div>
		      <div class="form-group has-feedback">
		          <label class="col-lg-3 control-label">密码</label>
		          <div class="col-lg-9">
		              <input data-bv-field="password" class="form-control" placeholder="请输入密码" name="password" id="pwd" type="password">
		          </div>
		      </div>
		      <div class="form-group has-feedback"></div>
		      <div class="row">
		          <label class="col-xs-2 control-label"></label>
		          <div class="col-sm-offset-1 col-xs-9">
		            <button type="submit" id="btn" class="btn btn-primary btn-block"  >登录</button>
		              
		          </div>
		          <label class="col-xs-2 control-label"></label>
		      </div>
    		</form>
    	</div>
  	 </div>
<@body.bodyEnd/>
	　
