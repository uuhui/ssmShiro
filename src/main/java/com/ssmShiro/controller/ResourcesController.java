package com.ssmShiro.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ssmShiro.entity.Resources;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.ResourcesService;

@RestController
@RequestMapping("/module")
public class ResourcesController {
	@Resource
	private ResourcesService resourceService;
	
	@RequestMapping("/list")
	public List<Resources> list(Resources entity) throws Exception {
		
		//return resourceService.getList(entity);
		 Map<String,Object> map = new HashMap<>();
	        map.put("type",1);
	        map.put("userid",1);
	        List<Resources> resourcesList = resourceService.loadUserResources(map);
	        return resourcesList;
	}
	@RequestMapping("/pageList")
	public PageInfo<Resources> pageList(PageInfo<Resources>page,Resources entity)throws Exception{
		return resourceService.selectByPage(page, entity);
	}
}
