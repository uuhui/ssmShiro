package com.ssmShiro.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssmShiro.entity.Role;
import com.ssmShiro.entity.UserRole;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.RoleService;
import com.ssmShiro.service.UserRoleService;

/**
 */
@Controller
@RequestMapping("/role")
public class RoleController {
	@Resource
	private RoleService roleService;
	@Resource
	private UserRoleService userRoleService;

	@RequestMapping("/list")
	@ResponseBody
	public List<Role> list(Role entity) throws Exception {
		return roleService.getList(entity);
	}

	@RequestMapping("/pageList")
	public PageInfo<Role> pageList(PageInfo<Role> page, Role entity) throws Exception {
		return roleService.selectByPage(page, entity);
	}

	@RequestMapping("/list2")
	public List<UserRole> list2(UserRole entity) throws Exception {
		return userRoleService.getList(entity);
	}

	@RequestMapping("/pageList2")
	public PageInfo<UserRole> pageList2(PageInfo<UserRole> page, UserRole entity) throws Exception {
		return userRoleService.selectByPage(page, entity);
	}

	@RequestMapping("/home")
	public String index() {
		return "login";
	}
}
