package com.ssmShiro;
//mapper-spring-boot-starter vserion的1.1.7版本或1.1.7之前的版本可以使用此包下的MapperScan
//import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//mapper-spring-boot-starter vserion的1.1.7之后需要使用此包下的MapperScan
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages="com.ssmShiro.dao")
public class bootApp {
public static void main(String[] args) {
	SpringApplication.run(bootApp.class, args);
	
}
}
