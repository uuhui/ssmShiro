package com.ssmShiro.service;

import java.util.List;
import java.util.Map;

import com.ssmShiro.entity.Resources;


/**
 * Created by yangqj on 2017/4/25.
 */
public interface ResourcesService extends BaseService<Resources> {
	/**
	 * 查询所有数据
	 * @return List<Resources>
	 * @throws Exception
	 */
	List<Resources>qeuryAll()throws Exception;

    List<Resources> loadUserResources(Map<String,Object> map);

    /*   List<Resources> queryResourcesListWithSelected(Integer rid);
     */
}
