package com.ssmShiro.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ssmShiro.dao.ResourcesMapper;
import com.ssmShiro.entity.Resources;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.ResourcesService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * Created by yangqj on 2017/4/25.
 */
@Service("resourcesService")
public class ResourcesServiceBean extends BaseServiceBean<Resources> implements ResourcesService {
	@Resource
	private ResourcesMapper resourcesMapper;

	@Override
	// @Cacheable(cacheNames="resources",key="#map['userid'].toString()+#map['type']")
	public List<Resources> loadUserResources(Map<String, Object> map) {
		return resourcesMapper.loadUserResources(map);
	}
	/*
	@Override
	public List<Resources> queryResourcesListWithSelected(Integer rid) {
		return resourcesMapper.queryResourcesListWithSelected(rid);
	}
*/
	@Override
	public List<Resources> getList(Resources entity) throws Exception {
		Example example=getExample(entity);
		example.setOrderByClause("parentid asc,sort asc");
		return selectByExample(example);
	}

	
	public Example getExample(Resources entity){
		Example example=new Example(Resources.class);
		if(entity!=null){
			Criteria criteria=example.createCriteria();
			if(StringUtils.hasText(entity.getName())){
				criteria.andLike("name", entity.getName().trim());
			}
			if(StringUtils.hasText(entity.getResurl())){
				criteria.andLike("resurl", entity.getResurl());
			}
			if(entity.getId()!=null){
				criteria.andEqualTo("id",entity.getId());
			}
			if(entity.getParentid()!=null){
				criteria.andEqualTo("parentid",entity.getParentid());
			}
			if(entity.getType()!=null){
				criteria.andEqualTo("type",entity.getType());
			}
		}
		return example;
	}
	@Override
	public PageInfo<Resources> selectByPage(PageInfo<Resources> page, Resources entity) throws Exception {
		return selectByPage(page, getExample(entity));
	}
	@Override
	public List<Resources> qeuryAll() throws Exception {
		return resourcesMapper.queryAll();
	}

}
