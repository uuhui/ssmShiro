package com.ssmShiro.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ssmShiro.dao.RoleMapper;
import com.ssmShiro.dao.RoleResourcesMapper;
import com.ssmShiro.entity.Role;
import com.ssmShiro.entity.RoleResources;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.RoleService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("roleService")
public class RoleServiceBean extends BaseServiceBean<Role> implements RoleService {

	@Resource
	private RoleMapper roleMapper;
	@Resource
	private RoleResourcesMapper roleResourcesMapper;

	@Override
	public List<Role> queryRoleListWithSelected(Integer uid) {
		return roleMapper.queryRoleListWithSelected(uid);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = { Exception.class })
	public void delRole(Integer roleid) {
		// 删除角色
		mapper.deleteByPrimaryKey(roleid);
		// 删除角色资源
		Example example = new Example(RoleResources.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andEqualTo("roleid", roleid);
		roleResourcesMapper.deleteByExample(example);

	}

	@Override
	public List<Role> getList(Role entity) throws Exception {
		
		return super.selectByExample(getExample(entity));
	}
   
	 @Override
	public Example getExample(Role entity) {
		 Example example = new Example(entity.getClass());
			if (example != null) {
				Criteria criteria = example.createCriteria();
				if (entity.getId() != null) {
					criteria.andEqualTo("id", entity.getId());
				}
			}
			return example;
	}

	@Override
	public PageInfo<Role> selectByPage(PageInfo<Role> page, Role entity) throws Exception {
		return super.selectByPage(page, getExample(entity));
	}
}
