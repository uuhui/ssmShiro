package com.ssmShiro.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.BaseService;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

@Service
public abstract class BaseServiceBean<T> implements BaseService<T> {
	@Autowired
    protected Mapper<T> mapper;

   
    @Override
    public T selectByKey(Object key) throws Exception {
    	return mapper.selectByPrimaryKey(key);
    }
    
    @Override
    public int add(T entity) throws Exception {
    	return mapper.insert(entity);
    }
    
    @Override
    public int del(Object key) throws Exception {
    	return mapper.deleteByPrimaryKey(key);
    }
    
    @Override
    public List<T> selectByExample(Object example) throws Exception {
    	return mapper.selectByExample(example);
    }
    /**
     * 根据对象生成Example查询对象
     * @param t 实体对象
     * @return Example
     */
    public abstract Example getExample(T t);
    	
    @Override
    public PageInfo<T> selectByPage(PageInfo<T> page, Example example ) throws Exception {
    	PageHelper.offsetPage(page.getStart(), page.getLimit());
    	com.github.pagehelper.PageInfo<T>pageInfo=new com.github.pagehelper.PageInfo<>(selectByExample(example));
    	page.setRows(pageInfo.getList());
    	page.setTotal(pageInfo.getTotal());
    	return page;
    }
}
