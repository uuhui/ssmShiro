package com.ssmShiro.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ssmShiro.entity.UserRole;
import com.ssmShiro.entity.dto.PageInfo;
import com.ssmShiro.service.UserRoleService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("userRoleService")
public class UserRoleServiceBean extends BaseServiceBean<UserRole> implements UserRoleService {

	@Override
	public List<UserRole> getList(UserRole entity) throws Exception {
		return super.selectByExample(getExample(entity));
	}

	@Override
	public Example getExample(UserRole entity) {
		Example example = new Example(entity.getClass());
		if (entity != null) {
			Criteria criteria = example.createCriteria();
			if (entity.getRoleid() != null) {
				criteria.andEqualTo("roleid", entity.getRoleid());
			}
			if (entity.getUserid() != null) {
				criteria.andEqualTo("userid", entity.getUserid());
			}
		}
		return example;
	}

	@Override
	public PageInfo<UserRole> selectByPage(PageInfo<UserRole> page, UserRole entity) throws Exception {
		return super.selectByPage(page, getExample(entity));
	}
}
