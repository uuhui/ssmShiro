package com.ssmShiro.service;

import com.ssmShiro.entity.User;

/**
 * Created by yangqj on 2017/4/21.
 */
public interface UserService extends BaseService<User>{
	/**
	 * 根据用户查询用户信息
	 * @param username
	 * @return
	 */
    User selectByUsername(String username)throws Exception;
    /**
     * 根据用户编号删除用户信息
     * @param userid 用户编号
     * @return int
     * @throws Exception
     */
    int delUser(Integer userid) throws Exception;
}
