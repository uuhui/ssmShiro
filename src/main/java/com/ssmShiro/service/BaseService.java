package com.ssmShiro.service;

import java.util.List;

import com.ssmShiro.entity.dto.PageInfo;

import tk.mybatis.mapper.entity.Example;

/**
  * 
  * @fileName: BaseService.java
  * @descrption: 通用接口类
  * @author: yinghui.zhang
  * @email: uuhui@163.com
  * @date:  2018年3月25日
  * @version: 1.0
  */
public interface BaseService <T>{
	/** 
	 * 根据主键查询数据
	 * @param key 主键
	 * @return T
	 */
	T selectByKey(Object key)throws Exception;
	/**
	 * 新增
	 * @param entity 新增对象
	 * @return int
	 * @throws Exception
	 */
	int add(T entity)throws Exception;
	/**
	 * 根据主键删除数据
	 * @param key 主键
	 * @return int
	 * @throws Exceptionn
	 */
	int del(Object key)throws Exception;
	/**
	 * 根据条件查询数据
	 * @param example 查询对象
	 * @return List<T>
	 * @throws Exception
	 */
	List<T>selectByExample(Object example)throws Exception;
	/**
	 * 根据条件查询数据数据
	 * @param entity 查询对象
	 * @return List<T>
	 * @throws Exception
	 */
	List<T>getList(T entity)throws Exception;
	/**
	 * 分页查询数据
	 * @param page 分页对象
	 * @param entity 查询对象
	 * @return PageInfo<T>
	 * @throws Exception
	 */
	PageInfo<T>selectByPage(PageInfo<T>page,T entity)throws Exception;
	
	/**
	 * 分页查询数据
	 * @param page 分页对象
	 * @param example 查询条件对象
	 * @return PageInfo<T>
	 * @throws Exception
	 */
	PageInfo<T>selectByPage(PageInfo<T>page,Example example)throws Exception;
}
