package com.ssmShiro.service;

import java.util.List;

import com.ssmShiro.entity.Role;

public interface RoleService  extends BaseService<Role> {

      List<Role> queryRoleListWithSelected(Integer uid);

    /**
     * 删除角色 同时删除角色资源表中的数据
     * @param roleid
     */
      void delRole(Integer roleid);
}
