package com.ssmShiro.entity.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @FileName: PageInfo.java
 * @Description: 数据分页实体类
 * @author: yinghui.zhang
 * @email: uuhui@163.com
 * @date: 2017年1月23日
 * @version: V1.0
 * @param <T>
 */
public class PageInfo<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 起始页
	private int start = 0;
	// 每页限制数据数
	private int limit = 30;
	// 排序名称
	private String sortName;
	// 排序方式
	private String sortOrder;
	// 总记录数
	private Long total;
	// 数据集合
	private List<T> rows = new ArrayList<T>();
	// 错误内容
	public String error;
	public boolean success = true;

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	@JsonIgnore
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@JsonIgnore
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	@JsonIgnore
	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	@JsonIgnore
	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

}
