package com.ssmShiro.dao;

import com.ssmShiro.entity.RoleResources;
import com.ssmShiro.util.MyMapper;

public interface RoleResourcesMapper extends MyMapper<RoleResources> {
}