package com.ssmShiro.dao;

import java.util.List;

import com.ssmShiro.entity.Role;
import com.ssmShiro.util.MyMapper;

public interface RoleMapper extends MyMapper<Role> {
    public List<Role> queryRoleListWithSelected(Integer id);
}