package com.ssmShiro.dao;

import java.util.List;
import java.util.Map;

import com.ssmShiro.entity.Resources;
import com.ssmShiro.util.MyMapper;

public interface ResourcesMapper extends MyMapper<Resources> {
	/**
	 * 查询数据数据
	 * @return
	 */
     List<Resources> queryAll();
   /**
    * 根据查询条件查询用户的资源信息
    * @param map 查询条件
    * @return List<Resources>
    */
    public List<Resources> loadUserResources(Map<String,Object> map);

    /**
     * 
     * @param rid
     * @return
     */
    public List<Resources> queryResourcesListWithSelected(Integer rid);
}