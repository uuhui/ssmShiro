package com.ssmShiro.dao;

import java.util.List;

import com.ssmShiro.entity.UserRole;
import com.ssmShiro.util.MyMapper;

public interface UserRoleMapper extends MyMapper<UserRole> {
    public List<Integer> findUserIdByRoleId(Integer roleId);
}