package com.ssmShiro.dao;

import com.ssmShiro.entity.User;
import com.ssmShiro.util.MyMapper;

public interface UserMapper extends MyMapper<User> {
}